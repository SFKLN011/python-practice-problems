# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:                                # solution
        return None                                     # solution
    largest = values[0]                                 # solution
    second_largest = values[0]                          # solution
    for value in values:                                # solution
        if value > largest:                             # solution
            second_largest = largest                    # solution
            largest = value                             # solution
        elif value > second_largest:                    # solution
            second_largest = value                      # solution
    return second_largest                               # solution
    # pass                                              # problem





""" def find_second_largest(values):
    list1 = [10, 20, 20, 4, 45, 45, 45, 99, 99]
    list2 = list(set(list1))
    return list2.sort()

# Printing the second last element


print(find_second_largest(list2[-2]))
 """

"""
def find_second_largest(values):
    if len(values) <= 1:                                # solution
        return None
    sort_value = sorted(values)
    largest = values[0]                                 # solution
    second_largest = values[1]


    # solution
    for value in values:                                # solution
        if sort_value and largest > second_largest:                             # solution
            second_largest = value                     # solution                               # solution
        elif sort_value and second_largest > largest:                    # solution
            second_largest = value                      # solution
    return second_largest                               # solution
    # pass                                              # problem

list = [10,100, 1,19]
 """
