# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    # num_attendees = the number of attendees
    num_attendees = len(attendees_list)
    # num_members = the number of memebrs
    num_members = len(members_list)
    # If the num_attendees divided by the num_members is
    # greater than 0.5
    if num_attendees >= num_members * .5:
        # return True
        return True
    # Otherwise
    else:
        # return False
        return False
