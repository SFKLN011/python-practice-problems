# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    # Using predefined function to
    # reverse to string print(s)
    reversed_word =''.join(reversed(word))

    # Checking if both string are
    # equal or not
    if (word == reversed_word):
    # return true if word is palindrome
        return True
    # return false if word is not palindrome
    return False



print(is_palindrome("dud"))
