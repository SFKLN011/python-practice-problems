# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# keyword def with function name calulcate_average passing one parameter of values
def calculate_average(values):
    #If there are no items in the list of values
    if len(values) == 0:
        # return None
        return None
    # sum = 0
    sum = 0
    # for each item in the list of values
    for item in values:
        sum = sum + item
    # return the sum divided by the number of items
    # in the list
    else:
        return sum / len(values)



list = [1, 2, 3]
print(calculate_average(list))
