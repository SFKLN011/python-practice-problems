# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if (                                # if statement
        'flour' in ingredients          # flour is in ingriendts
        and 'eggs' in ingredients       # eggs is in ingriendts
        and 'oil' in ingredients        # oil is in ingriendts
    ):
        return True                     #return True if all ingriendts are in the function
    return False                        #return False if that is not the case



print(can_make_pasta(["eggs", "flour",  "oil"]))
